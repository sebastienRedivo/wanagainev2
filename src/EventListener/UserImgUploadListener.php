<?php

namespace App\EventListener;


use App\Entity\User;
use App\Service\FileUploader;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UserImgUploadListener
{
    private $uploader;

    public function __construct(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
         
        $entity = $args->getEntity();
       
        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();
        
        
        $this->uploadFile($entity);
    }

    private function uploadFile($entity)
    {  
        // upload only works for User entities
        if (!$entity instanceof User) {
            return;
        }
       
        $file = $entity->getImage();
            
        // only upload new files
        if ($file instanceof UploadedFile) {
         
            $fileName = $this->uploader->upload($file); 
       
            $entity->setImage($fileName);
           
        } elseif ($file instanceof File) {    
            // prevents the full file path being saved on updates
            // as the path is set on the postLoad listener
            dump($file);
            $entity->setImage($file->getFilename());
        }

    }

    public function postLoad(LifecycleEventArgs $args)
    {  
        $entity = $args->getEntity();
        
        if (!$entity instanceof User) {
            return;
        }

        if ($fileName = $entity->getImage()) {
            $entity->setImage(new File($this->uploader->getTargetDirectory().'/'.$fileName));
        }
    }
}