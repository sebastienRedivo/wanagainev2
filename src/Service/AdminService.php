<?php

namespace App\Service;

use App\Repository\ContactRepository;
use App\Repository\ComentsRepository;

class AdminService
{
    
    
    private $newMessage;
    private $viewComments;

    public function __construct(ContactRepository $contactRepository, ComentsRepository $comentsRepository)
    {
        $this->newMessage = $contactRepository;
        $this->viewComments = $comentsRepository;
        
    }

    // recupére les messages dans la BDD à l'aide du repository personnalisé
    public function viewNewMessage()
    {
        // affiche seulement les messages reçu qui ne sont pas lu
        return $this->newMessage->findByNotView();
    }

    public function viewLastComments(){

        return $this->viewComments->findByLastComments();
    }

}