<?php

namespace App\Form;

use App\Entity\CategoryBlog;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CategoryBlogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,  ['label' => 'titre'])
            ->add('description', TextType::class,  ['label' => 'description'])
            ->add('imageFile', FileType::class, ['required' => false, 'label' => 'Image de la catégorie', 'attr'=>['onchange'=>'readURL(this);']])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CategoryBlog::class,
        ]);
    }
}
