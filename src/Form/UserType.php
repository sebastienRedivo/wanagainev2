<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TelType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            
            
          
            ->add('name', TextType::class,  ['label' => 'nom'])
            ->add('lastname', TextType::class,  ['label' => 'prénom'])
            ->add('phone', TelType::class, ['label' => 'téléphone' , 'attr'=>['pattern'=>'[0-9]{10}']])
            ->add('city', TextType::class,  ['label' => 'ville'])
            ->add('zipCode', TextType::class,  ['label' => 'code postal'])
            ->add('pays')
            ->add('adress')
          
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_protection' => false,
        ]);
    }
}
