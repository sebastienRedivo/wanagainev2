<?php

namespace App\Form;

use App\Entity\CategoryMenu;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CategoryMenuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,  ['label' => 'titre'])
            ->add('description', TextType::class,  ['label' => 'description', 'required' => false])
            ->add('imageFile', FileType::class, ['required' => false, 'label' => 'Image de la catégorie'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CategoryMenu::class,
        ]);
    }
}
