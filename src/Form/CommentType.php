<?php

namespace App\Form;

use App\Entity\Coments;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author', TextType::class, array('label' => 'Nom d\'utilisateur', 'attr' => array('placeholder'=>'nom')))
            ->add('content', TextareaType::class, array('label' => 'Commentaire', 'attr' => array('placeholder' => 'commentaire')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Coments::class,
        ]);
    }
}
