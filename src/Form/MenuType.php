<?php

namespace App\Form;

use App\Entity\Menu;
use App\Entity\CategoryMenu;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class MenuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,  ['label' => 'Nom du menu'])
            ->add('subDescription', TextType::class,  ['label' => 'description secondaire'])
            ->add('content', TextType::class,  ['label' => 'contenu'])
            ->add('price', TextType::class,  ['label' => 'Prix'])
            ->add('CategoryMenu', EntityType::class, array(
                'class'        => CategoryMenu::class,
                'choice_label' => 'name',
                'multiple'     => false,
                'label'        => 'catégorie du menu'
                ))
            ->add('imageFile', FileType::class, ['required' => false, 'label' => 'Image du menu','attr'=>['onchange'=>'readURL(this);']])
            ->add('publish', CheckboxType::class, ['required' => false,'label' => 'publier'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Menu::class,
        ]);
    }
}
