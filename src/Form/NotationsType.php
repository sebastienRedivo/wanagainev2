<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Notations;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class NotationsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', TextareaType::class, array('label' => 'Commentaire', 'attr' => array('placeholder' => 'commentaire')))
            ->add('vote', ChoiceType::class, array(
                'choices' => array(
                    '1 = Mauvais' => '1',
                    '2 = Moyens' => '2',
                    '3 = Bien' => '3',
                    '4 = Trés bien' => '4',
                    '5 = Extra' => '5'
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Notations::class,
        ]);
    }
}
