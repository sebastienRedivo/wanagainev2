<?php

namespace App\Form;

use App\Entity\Blog;
use App\Entity\CategoryBlog;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class BlogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,  ['label' => 'titre'])
            ->add('imageFile', FileType::class, ['required' => false, 'label' => 'Image du blog', 'attr'=>['onchange'=>'readURL(this);']])
            ->add('subTitle', TextType::class,  ['label' => 'sous-titre', 'required' => false])
            ->add('CategoryBlog', EntityType::class, array(
                'class'        => CategoryBlog::class,
                'choice_label' => 'title',
                'multiple'     => false,
                'label' => 'categorie'
            ))
            ->add('content', CKEditorType::class, array(
                'attr' => array('class' => 'tinymce'),
                'label' => 'contenu'
        
            ))
            
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Blog::class,
        ]);
    }
}
