<?php
namespace App\Security;

use App\Form\RegisterType;
use App\Controller\UserController;
use App\Entity\User; // your user entity
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Component\HttpFoundation\RedirectResponse;
use KnpU\OAuth2ClientBundle\Client\Provider\FacebookClient;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use KnpU\OAuth2ClientBundle\Security\Authenticator\SocialAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;


class FacebookAuthenticator extends SocialAuthenticator
{
    private $clientRegistry;
    private $em;

    public function __construct(ClientRegistry $clientRegistry, EntityManagerInterface $em)
    {
        $this->clientRegistry = $clientRegistry;
        $this->em = $em;
    }

    public function supports(Request $request)
    {
        // continue ONLY if the current ROUTE matches the check ROUTE
        return $request->attributes->get('_route') === 'connect_facebook_check';
    }

    public function getCredentials(Request $request)
    {
        // this method is only called if supports() returns true

        // For Symfony lower than 3.4 the supports method need to be called manually here:
        // if (!$this->supports($request)) {
        //     return null;
        // }

        return $this->fetchAccessToken($this->getFacebookClient());
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        /** @var FacebookUser $facebookUser */
        $facebookUser = $this->getFacebookClient()
            ->fetchUserFromToken($credentials);
        $user=[''];
        $email = $facebookUser->getEmail();
        if($email===null){
            $email = $facebookUser->getId().'@hotmail.fr';
        }
        $idFacebook= $facebookUser->getId();
        // dd($idFacebook);
        // 1) have they logged in with Facebook before? Easy!
        $existingUser = $this->em->getRepository(User::class)
            ->findOneBy(['facebookToken' => $idFacebook]);

        if ($existingUser) {
            
            return $existingUser;
        }else{
            
            $user = new User();
            $mdp= '$argon2i$v=19$m=1024,t=2,p=2$aHJHVzJrT0VnME12WGp2aw$opQREgdBB9Tj12syqNpfSF8hqhNGJa88jSLWigRKIPs';
            $role= ["ROLE_USER"];
            $user->setFacebookToken($idFacebook)
                ->setRoles($role)
                ->setPassword($mdp)
                ->setUsername($email)
                ->setEmail($email);
            // Enregistre le membre en base
            $this->em->persist($user);
            $this->em->flush();
            // dd('pas de token');
            
          return $user;
            
           
        }

        // 2) do we have a matching user by email?
        // $user = $this->em->getRepository(User::class)
        //             ->findOneBy(['email' => $email]);

        // 3) Maybe you just want to "register" them by creating
        // a User object
     
        
        
    }

    /**
     * @return FacebookClient
     */
    private function getFacebookClient()
    {
        return $this->clientRegistry
            // "facebook_main" is the key used in config/packages/knpu_oauth2_client.yaml
            ->getClient('facebook_main');
	}

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // on success, let the request continue
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
		$message = strtr($exception->getMessageKey(), $exception->getMessageData());

        return new Response($message, Response::HTTP_FORBIDDEN);
    }

    /**
     * Called when authentication is needed, but it's not sent.
     * This redirects to the 'login'.
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new RedirectResponse(
            '/login/', // might be the site, where users choose their oauth provider
            Response::HTTP_TEMPORARY_REDIRECT
        );
    }

    // ...
}