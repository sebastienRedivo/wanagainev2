<?php

namespace App\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BlogRepository")
 * @Vich\Uploadable
 */
class Blog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $subTitle;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CategoryBlog", inversedBy="blogs")
     */
    private $CategoryBlog;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="BlogUser")
     */
    private $user;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var File|null
     * 
     * @Vich\UploadableField(mapping="imag_ad", fileNameProperty="image")
     */
    private $imageFile;

    /**
     * @ORM\Column(type="integer")
     */
    private $counter;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Coments", mappedBy="blog", orphanRemoval=true)
     */
    private $coments;

    /**
     * Undocumented function
     *
     * @param File|null $imageFile
     * 
     */
    public function setImageFile(?File $imageFile)
    {
        $this->imageFile = $imageFile;
        // on verifie que  c'est bien une instance de UploadedFile si c'est le cas on met à jours
        if ($this->imageFile instanceof UploadedFile ) {
            // on met à jours le champ update
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return File|null
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->coments = new ArrayCollection(); 
        // si il y a une création d'un article
        if($this->createdAt){
            // je met par défault le counter à 0
            $this->counter = 0;
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): string
    {
        return(new Slugify())->slugify($this->title);
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getSubTitle(): ?string
    {
        return $this->subTitle;
    }

    public function setSubTitle(?string $subTitle): self
    {
        $this->subTitle = $subTitle;

        return $this;
    }

    public function getCategoryBlog(): ?CategoryBlog
    {
        return $this->CategoryBlog;
    }

    public function setCategoryBlog(?CategoryBlog $CategoryBlog): self
    {
        $this->CategoryBlog = $CategoryBlog;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {

            $this->image = $image;
        
        return $this;
    }

    public function getCounter(): ?int
    {
        return $this->counter;
    }

    public function setCounter(int $counter): self
    {
        $this->counter = $counter;

        return $this;
    }

    /**
     * @return Collection|Coments[]
     */
    public function getComents(): Collection
    {
        return $this->coments;
    }

    public function addComent(Coments $coment): self
    {
        if (!$this->coments->contains($coment)) {
            $this->coments[] = $coment;
            $coment->setBlog($this);
        }

        return $this;
    }

    public function removeComent(Coments $coment): self
    {
        if ($this->coments->contains($coment)) {
            $this->coments->removeElement($coment);
            // set the owning side to null (unless already changed)
            if ($coment->getBlog() === $this) {
                $coment->setBlog(null);
            }
        }

        return $this;
    }

}
