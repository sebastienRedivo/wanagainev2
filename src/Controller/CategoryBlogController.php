<?php

namespace App\Controller;

use App\Entity\CategoryBlog;
use App\Form\CategoryBlogType;
use App\Repository\BlogRepository;
use App\Repository\CategoryBlogRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/category/blog")
 */
class CategoryBlogController extends AbstractController
{
    /**
     * @Route("/", name="category_blog_index", methods="GET")
     */
    public function index(CategoryBlogRepository $categoryBlogRepository): Response
    {
        return $this->render(
            'category_blog/index.html.twig',
            [
                'category_blogs' => $categoryBlogRepository->findAll(),
            ]
        );
    }

    /**
     * @Route("/admin/", name="admin_category_blog_index", methods="GET")
     */
    public function indexAdmin(CategoryBlogRepository $categoryBlogRepository): Response
    {
        return $this->render(
            'admin/category_blog/index.html.twig',
            [
                'category_blogs' => $categoryBlogRepository->findAll(),
            ]
        );
    }

    /**
     * @Route("/admin/new", name="admin_category_blog_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $categoryBlog = new CategoryBlog();
        $form = $this->createForm(CategoryBlogType::class, $categoryBlog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($categoryBlog);
            $em->flush();

            return $this->redirectToRoute('admin_category_blog_index');
        }

        return $this->render('admin/category_blog/new.html.twig', [
            'category_blog' => $categoryBlog,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="category_blog_show", methods="GET")
     */
    public function show(CategoryBlog $categoryBlog, BlogRepository $blog)
    {
        return $this->render('category_blog/show.html.twig', [
            'category_blog' => $categoryBlog,
            'blog' => $blog->findAll()
        ]);
    }

    /**
     * @Route("/admin/{id}/edit", name="admin_category_blog_edit", methods="GET|POST")
     */
    public function edit(Request $request, CategoryBlog $categoryBlog): Response
    {
        $form = $this->createForm(CategoryBlogType::class, $categoryBlog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_category_blog_index', ['id' => $categoryBlog->getId()]);
        }

        return $this->render('admin/category_blog/edit.html.twig', [
            'category_blog' => $categoryBlog,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/{id}", name="admin_category_blog_delete", methods="DELETE")
     */
    public function delete(Request $request, CategoryBlog $categoryBlog): Response
    {
        if ($this->isCsrfTokenValid('delete' . $categoryBlog->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($categoryBlog);
            $em->flush();
        }

        return $this->redirectToRoute('admin_category_blog_index');
    }
}
