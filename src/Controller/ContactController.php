<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\ContactRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/contact")
 */
class ContactController extends AbstractController
{
    /**
     * @Route("/admin/", name="admin_contact_index", methods={"GET"})
     */
    public function index(ContactRepository $contactRepository): Response
    {
        return $this->render('admin/contact/index.html.twig', [
            'contacts' => $contactRepository->findBy([],['sendAt'=> 'desc']),
       
        ]);
    }

    /**
     * @Route("/new", name="contact_new", methods={"POST"})
     */
    public function new(Request $request): Response
    {
        
       
        $contact = new Contact();
   
        //$form = $this->createForm(ContactType::class, $contact);
        //$form->handleRequest($request);
        if ($request->getMethod('post')) {
            $contact->setContent($request->request->get('content'))
                    ->setMailContact($request->request->get('mailContact'))
                    ->setName($request->request->get('name'))
                    ->setPhone($request->request->get('phone'))
                    ->setSubject($request->request->get('subject'));
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contact);
            $entityManager->flush();

            return $this->redirectToRoute('home');
        }

      
    }

    /**
     * @Route("/admin/{id}", name="admin_contact_show", methods={"GET"})
     */
    public function show(Contact $contact): Response
    {
        return $this->render('admin/contact/show.html.twig', [
            'contact' => $contact,
        ]);
    }

    /**
     * @Route("/admin/{id}", name="admin_contact_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Contact $contact): Response
    {
        if ($this->isCsrfTokenValid('delete'.$contact->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($contact);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_contact_index');
    }

      /**
     * @Route("/add_view/{id}", name="contact_view", methods="GET")
     * Ajoute une vue pour le blog
     */
    public function addView(Contact $contact)
    {

        $em = $this->getDoctrine()->getManager(); 
        $contact->setView(true);
        //puis je transmet
        $em->flush();

        return $this->redirectToRoute('contact_index');
    }
}
