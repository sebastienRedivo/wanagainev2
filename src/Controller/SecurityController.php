<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterType;
use App\Repository\MenuRepository;
use App\Repository\PicturesRepository;
use App\Repository\NotationsRepository;
use App\Repository\CategoryMenuRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SecurityController extends AbstractController
{

    /**
     * page d'acceuil du site
     * @Route("/", name="home")
     */
    public function home(NotationsRepository $notes, PicturesRepository $pictures, MenuRepository $menuRepository, CategoryMenuRepository $menuCategories)
    {
        $votes = $notes->findAll();
        $cateMenu = $menuCategories->findAll();
        $pictures = $pictures->findAll();
        $menu = $menuRepository->findAll();
        return $this->render(
            'base.html.twig',
            array(
                'error' => null,
                'votes' => $votes,
                'pictures' => $pictures,
                'menus' => $menu,
                'categories' => $cateMenu
            )
        );
    }

    /**
     * page contact
     * @Route("/contact", name="contact")
     */
    public function contact()
    {


        return $this->render(
            'contact/contact.html.twig',
            array(
                'error' => null,

            )
        );
    }

    /**
     * gére la connexion
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * gére l'inscription de l'utilisateur
     * @Route("/inscription", name="signIn")
     */
    public function registerAction(\Swift_Mailer $mailer, Request $request, UserPasswordEncoderInterface $encoder, ObjectManager $em)
    {
        // création du formulaire
        $user = new User();
        // instancie le formulaire avec les contraintes par défaut, + la contrainte registration pour que la saisie du mot de passe soit obligatoire
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);
        $role = ["ROLE_USER"];

        if ($form->isSubmitted() && $form->isValid()) {

            // Encode le mot de passe
            $hash = $encoder->encodePassword($user, $user->getPassword());
            //envoi le mot de passe encoder
            $user->setPassword($hash);
            $user->setCreatedAt(new \DateTime());
            //definie le role 
            $user->setRoles($role);

            // Enregistre le membre en base
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('app_login');
        }

        //crée la vue du formulaire
        return $this->render(
            'security/register.html.twig',
            array(
                'form' => $form->createView(),
                'error' => null
            )
        );
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(Session $session)
    {

        // set flash messages
        return $this . redirectToroute('app_login')->$session->getFlashBag()->add('notice', 'vous avez été deconnecter');
    }

     /**
     * @Route("/resetPassword", name="reset_password")
     */
    public function resetPassword(Request $request, \Swift_Mailer $mailer, TokenGeneratorInterface $tokenGenerator): Response
    {
        if ($request->isMethod('POST')) {
 
            $email = $request->request->get('email');
 
            $entityManager = $this->getDoctrine()->getManager();
            $user = $entityManager->getRepository(User::class)->findOneByEmail($email);

 
            if ($user === null) {
                $this->addFlash('danger', 'Email Inconnu');
                return $this->redirectToRoute('home');
            }
            $token = $tokenGenerator->generateToken();
 
            try{
                $user->setResetToken($token);
                $entityManager->flush();
            } catch (\Exception $e) {
                $this->addFlash('ATTENTION', $e->getMessage());
                return $this->redirectToRoute('home');
            }
            $url = $this->generateUrl('new_reset_password', array('token' => $token), UrlGeneratorInterface::ABSOLUTE_URL);
            
            $message = (new \Swift_Message('mot de passe oublié'))
                ->setFrom(['stw34500@gmail.com' => 'symfo'])
                ->setTo($user->getEmail())
                ->setBody(
                    $this->renderView(          
                        'mails/reset_password.html.twig',
                        ['url' => $url]
                    ),
                    'text/html'
                );
 
            $mailer->send($message);
 
            $this->addFlash('notice', 'Mail envoyé');
 
            return $this->redirectToRoute('home');
        }
        return $this->render('security/reset_password.html.twig');
    }

    /**
     * @Route("/reset_password/{token}", name="new_reset_password")
     */
    public function newResetPassword(Request $request, string $token, UserPasswordEncoderInterface $passwordEncoder)
    {
 
        if ($request->isMethod('POST')) {
            $entityManager = $this->getDoctrine()->getManager();
 
            $user = $entityManager->getRepository(User::class)->findOneByResetToken($token);
 
            if ($user === null) {
                $this->addFlash('danger', 'Token Inconnu');
                return $this->redirectToRoute('homepage');
            }
 
            $user->setResetToken(null);
            $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('password')));
            $entityManager->flush();
 
            $this->addFlash('notice', 'Mot de passe mis à jour');
 
            return $this->redirectToRoute('home');
        }else {
 
            return $this->render('security/new_password.html.twig', ['token' => $token]);
        }
 
    }
    
    
}
