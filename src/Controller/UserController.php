<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\NotationsRepository;
use App\Repository\UserRepository;
use App\Repository\PicturesRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/admin/", name="admin_user_index", methods="GET")
     */
    public function index(UserRepository $userRepository, PicturesRepository $picturesRepository): Response
    {
        $imageUsers = $picturesRepository->findAll();
        $users = $userRepository->findAll();

        return $this->render('admin/user/index.html.twig', [
            'users' => $users,
            'images' => $imageUsers
        ]);
    }

    /**
     * @Route("/admin/new", name="admin_user_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * affiche le profile
     * @Route("/member/{id}", name="user_show", methods="GET")
     */
    public function show(NotationsRepository $notation, User $user, UserRepository $userRepository, PicturesRepository $picturesRepository): Response
    {
        
        // si l'utilisateur à le Role ADMIN
        if ($this->isGranted('ROLE_ADMIN') && $this->getUser()!= $user) {
            $imageUser = $picturesRepository->findOneBy(['user' => $user]);
            if (!isset($imageUser)) {
                $imageUser = null;
            }

            $user = $userRepository->find($user);
            return $this->render('admin/user/show.html.twig', ['user' => $user]);
        }

        // si l'utisateur demander dans la page est bien l'utilisateur qui la demande
        if ($user == $this->getUser()) {
            $imageUser = $picturesRepository->findOneBy(['user' => $this->getUser()]);
            $note = $notation->findOneBy(['users' => $user]);
            if (!isset($imageUser)) {
                $imageUser = null;
            }

            return $this->render('user/show.html.twig', ['user' =>  $this->getUser(), 'image' => $imageUser, 'note' => $note]);
        }

       
        // sinon je redirige l'utilisateur à la page d'acceuil
        return $this->redirectToRoute('home');
    }

    /**
     * edition du profile
     * @Route("/member/{id}/edit", name="user_edit", methods="GET|POST")
     */
    public function edit(Request $request, User $user): Response
    {
        // si l'user est bien celui qui fait la demade pour la modif ou si l'utilisateur a le role ADMIN
        if ($user == $this->getUser() || $this->isGranted('ROLE_ADMIN')) {

            $form = $this->createForm(UserType::class, $user);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $user->setUpdatedAt(new \DateTime);

                $this->getDoctrine()->getManager()->flush();
                // redirige l'utilisateur sur le profile modifier 
                return $this->redirectToRoute('user_show', ['id' => $user->getId()]);
            }
        } else {
            // sinon je redirige
            return $this->redirectToRoute('user_show', ['id' => $user->getId()]);
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),

        ]);
    }

    /**
     * @Route("/member/{id}", name="user_delete", methods="DELETE")
     */
    public function delete(Request $request, User $user): Response
    {
        // je verifie que l'utilisateur es tbien celui qui correspond à l'user qui veut supprimer ou si c'est nbien un admin
        if ($user == $this->getUser() || $this->isGranted('ROLE_ADMIN')) {
            if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($user);
                $em->flush();
            }
        }
        return $this->redirectToRoute('user_index');
    }
}
