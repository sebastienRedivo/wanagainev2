<?php

namespace App\Controller;


use App\Entity\Pictures;
use App\Form\PicturesType;
use App\Repository\PicturesRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/pictures")
 */
class PicturesController extends AbstractController
{
    /**
     * @Route("/admin/", name="pictures_index", methods={"GET"})
     */
    public function index(PicturesRepository $picturesRepository): Response
    {
        dump($picturesRepository->findAll());
        return $this->render('pictures/index.html.twig', [
            'pictures' => $picturesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/member/new", name="pictures_new", methods="GET|POST")
     * @Route("/member/{id}/edit", name="pictures_edit", methods={"GET","POST"})
     */
    public function new(Request $request, Pictures $picture = null, PicturesRepository $picturesRepository)
    {
        /**
         * récupere l'objet Pictures qui est en lien avec l'utilisateur qui le traite
         */
        $presenceUser = $picturesRepository->findOneBy(['user' => $this->getUser()]);
        /**
         * si l'utilisateur est présent on lui passe l'objet dans la variable $picture qui récupére le contenu
         */
        if ($presenceUser) {
            $picture = $presenceUser;
        }

        if (!$picture) {
            $picture = new Pictures();
        }

        /**
         * je controle que l'utilisateur est bien celui qui traite la demande en récuperant le token
         */
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $form = $this->createForm(PicturesType::class, $picture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * si l'utilisateur a pas de d'image c'est que l'on fait un créate alors on lui en l'utilisateur dans l'objet
             */
            if (empty($presenceUser)) {
                $picture->setUser($user);
            }


            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($picture);

            $entityManager->flush();

            return $this->redirectToRoute('user_show', ['id' => $user->getId()]);
        }

        return $this->render('pictures/new.html.twig', [
            'picture' => $picture,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pictures_show", methods={"GET"})
     */
    public function show(Pictures $picture): Response
    {
        if ($this->getUser() == $picture->getUser() && $this->isGranted('ROLE_USER') || $this->isGranted('ROLE_ADMIN')) {
            return $this->render('pictures/show.html.twig', [
                'picture' => $picture,
            ]);
        } else {
            dump("ce n'est pas ta photo");
        }
    }

    /**
     * @Route("/member/{id}", name="pictures_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Pictures $picture): Response
    {
        if ($this->isCsrfTokenValid('delete' . $picture->getId(), $request->request->get('_token')) && $this->getUser() == $picture->getUser() && $this->isGranted('ROLE_USER')) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($picture);
            $entityManager->flush();
        }

        return $this->redirectToRoute('pictures_index');
    }
}
