<?php

namespace App\Controller;

use App\Entity\CategoryMenu;
use App\Form\CategoryMenuType;
use App\Repository\CategoryMenuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/category/menu")
 */
class CategoryMenuController extends AbstractController
{
    /**
     * @Route("/admin/", name="admin_category_menu_index", methods="GET")
     */
    public function index(CategoryMenuRepository $categoryMenuRepository): Response
    {
        return $this->render('admin/category_menu/index.html.twig', ['category_menus' => $categoryMenuRepository->findAll()]);
    }

    /**
     * @Route("/admin/new", name="admin_category_menu_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $categoryMenu = new CategoryMenu();
        $form = $this->createForm(CategoryMenuType::class, $categoryMenu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($categoryMenu);
            $em->flush();

            return $this->redirectToRoute('category_menu_index');
        }

        return $this->render('/admin/category_menu/new.html.twig', [
            'category_menu' => $categoryMenu,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/{id}", name="category_menu_show", methods="GET")
     */
    public function show(CategoryMenu $categoryMenu): Response
    {
        return $this->render('category_menu/show.html.twig', ['category_menu' => $categoryMenu]);
    }

    /**
     * @Route("/admin/{id}/edit", name="admin_category_menu_edit", methods="GET|POST")
     */
    public function edit(Request $request, CategoryMenu $categoryMenu): Response
    {
        $form = $this->createForm(CategoryMenuType::class, $categoryMenu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_category_menu_index', ['id' => $categoryMenu->getId()]);
        }

        return $this->render('admin/category_menu/edit.html.twig', [
            'category_menu' => $categoryMenu,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/{id}", name="admin_category_menu_delete", methods="DELETE")
     */
    public function delete(Request $request, CategoryMenu $categoryMenu): Response
    {
        if ($this->isCsrfTokenValid('delete' . $categoryMenu->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($categoryMenu);
            $em->flush();
        }

        return $this->redirectToRoute('category_menu_index');
    }
}
