<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ContactRepository;

/**
 * @Route("/administrator")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/admin/", name="admin")
     */
    public function index(ContactRepository $contactRepository, UserRepository $compteUser)
    {
        return $this->render('admin/admin.html.twig', [
            //'controller_name' => 'AdminController',
            'countUser'=> $compteUser->findAll(),
            'contacts' => $contactRepository->findByNotView(),
        ]);
    }

    /**
     * @Route("/cal", name="cal")
     */
    public function calcul(){
        $valide = true;
        $array1 = array();

        for($j=0; $j <= 20; $j++){
            
            $verif=false;
                $array2 = array();
                for($i=0; $i<=4; $i++){
                    
                    $bol = false;
                    
                    while(!$bol){
                        
                        $val= rand(1,50);
                        
                        if(!in_array($val,$array2)){
                            
                            $bol=true;
                        }
                        
                    }
                        $array2[$i] = $val;
                        
                }

            
            asort($array2);
            
            $array1[$j] = $array2;
        }

        asort($array1);
      /*      $l = 0;
        foreach( $array1 as $array ) {
            $l++;
            echo "<br><h2>ticket $l </h2>";

            foreach ( $array as $key => $value ) {
                echo "- $value ";
            }
            
        }*/
        return $this->render('admin/index.html.twig', [
            'result' => $array1,
            'controller_name' => 'AdminController',
        ]);
    }

  


}
