<?php

namespace App\Controller;

use App\Entity\Coments;
use App\Form\ComentsType;
use App\Repository\ComentsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/coments/admin")
 */
class ComentsController extends AbstractController
{
    /**
     * @Route("/", name="admin_coments_index", methods={"GET"})
     */
    public function index(ComentsRepository $comentsRepository): Response
    {
        return $this->render('admin/coments/index.html.twig', [
            'coments' => $comentsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_coments_show", methods={"GET"})
     */
    public function show(Coments $coment): Response
    {
        return $this->render('admin/coments/show.html.twig', [
            'coment' => $coment,
        ]);
    }

    /**
     * @Route("/{id}", name="admin_coments_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Coments $coment): Response
    {
        if ($this->isCsrfTokenValid('delete'.$coment->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($coment);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_coments_index');
    }
}
