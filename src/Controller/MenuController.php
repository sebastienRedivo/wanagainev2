<?php

namespace App\Controller;

use App\Entity\Menu;
use App\Form\MenuType;

use App\Repository\MenuRepository;
use App\Repository\CategoryMenuRepository;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/menu")
 */
class MenuController extends AbstractController
{
    /**
     * @Route("/admin/", name="admin_menu_index", methods="GET")
     */
    public function index(MenuRepository $menuRepository): Response
    {
        return $this->render('admin/menu/index.html.twig', ['menus' => $menuRepository->findAll()]);
    }

    /**
     * @Route("/admin/new", name="admin_menu_new", methods="GET|POST")
     */
    public function new(Request $request, ObjectManager $em): Response
    {
        $menu = new Menu();

        $form = $this->createForm(MenuType::class, $menu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($menu);
            $em->flush();

            return $this->redirectToRoute('admin_menu_index');
        }

        return $this->render('admin/menu/new.html.twig', [
            'menu' => $menu,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/{id}", name="admin_menu_show", methods="GET")
     */
    public function show(Menu $menu): Response
    {
        return $this->render('admin/menu/show.html.twig', ['menu' => $menu]);
    }

    /**
     * @Route("/admin/{id}/edit", name="admin_menu_edit", methods="GET|POST")
     */
    public function edit(Request $request, Menu $menu): Response
    {
        $form = $this->createForm(MenuType::class, $menu);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_menu_index', ['id' => $menu->getId()]);
        }

        return $this->render('admin/menu/edit.html.twig', [
            'menu' => $menu,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/{id}", name="admin_menu_delete", methods="DELETE")
     */
    public function delete(Request $request, Menu $menu): Response
    {
        if ($this->isCsrfTokenValid('delete' . $menu->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($menu);
            $em->flush();
        }

        return $this->redirectToRoute('admin_menu_index');
    }

    /**
     * @Route("/", name="menu_public_index", methods="GET")
     */
    public function public_index(MenuRepository $menuRepository, CategoryMenuRepository $menuCategories): Response
    {
        return $this->render(
            'menu/public_show_index.html.twig',
            [
                'menus' => $menuRepository->findAll(),
                'categories' => $menuCategories->findAll()
            ]
        );
    }
}
