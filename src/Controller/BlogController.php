<?php

namespace App\Controller;

use App\Entity\Blog;
use App\Form\BlogType;
use App\Entity\Coments;
use App\Form\CommentType;
use App\Entity\CategoryBlog;
use App\Repository\BlogRepository;
use App\Repository\UserRepository;
use App\Repository\CategoryBlogRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/blog")
 */
class BlogController extends AbstractController
{


    /**
     * @Route("/admin/", name="admin_blog_index", methods="GET")
     * 
     * Liste tout les blogs pour l'admin
     */
    public function index(Request $request, PaginatorInterface $paginator,BlogRepository $blogRepository, UserRepository $userRepo): Response
    {
        $page = $paginator->paginate(
            $blogRepository->findBy([],['createdAt'=> 'desc']),
            $request->query->getInt('page', 1),
            12
    
        );
        return $this->render(
            'admin/blog/index.html.twig',
            [
                'blogs' => $page,
                'user' => $userRepo->findAll()
            ]
        );
    }

    /**
     * @Route("/admin/new", name="admin_blog_new", methods="GET|POST")
     * 
     * Ajoute un nouveau blog
     * 
     */
    public function new(Request $request)
    {
        // initialisation du nouveau blog
        $blog = new Blog();
        //appelle du formulaire qui contient les informations du blog je lui met l'entité dans le parametre
        $form = $this->createForm(BlogType::class, $blog);
        $form->handleRequest($request);

        // verification du formulaire si il est soumis et si il est valide
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            //ajout de l'utilisateur qui ajoute le blog
            $blog->setUser($this->getUser());
            // je persist les données avant de les envoyer
            $em->persist($blog);
            //j'envois toute les données transmise 
            $em->flush();
            // je redirige l'utilisateur à la liste de tout les blogs
            return $this->redirectToRoute('admin_blog_index');
        }
        // je transmet le formulaire et l'entitée à la vue
        return $this->render('admin/blog/new.html.twig', [
            'blog' => $blog,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{slug}-{id}", name="blog_show", methods="GET|POST", requirements={"slug": "[a-z0-9\-]*", "id": "[0-9]*"})
     * 
     */
    public function show(Blog $blog, Request $request, UserRepository $userRepo, CategoryBlogRepository $blogCategories)
    {

        // récuperation de toutes les categories des blogs
        $categories = $blogCategories->findAll();
        // recuperation de tout les utilisateurs
        $user = $userRepo->findAll();

        // initialisation de l'entité Coments qui permet de traiter le formulaire des commentaire
        $comment = new Coments();
        //déclaration du formulaire pour les commentaires
        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);
        // si les informations du formulaire sont soumis et valide
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // je transmet la date à aujourd'hui et je transmet le blog pour le lié 
            $comment->setCreatedAt(new \DateTime())
                ->setBlog($blog);
            // persist les données        
            $em->persist($comment);
            // transmet les données à la base de donnée
            $em->flush();

            // redirection sur le blog en cour
            return $this->redirectToRoute('blog_show', ['id' => $blog->getId()]);
        }

        // renvoie le formulaire , les categories et les users à la vue
        return $this->render('blog/show.html.twig', [
            'blog' => $blog,
            'form' => $form->createView(),
            'categories' => $categories,
            'user' => $user

        ]);
    }

    /**
     * @Route("/admin/{id}/edit", name="admin_blog_edit", methods="GET|POST")
     */
    public function edit(Request $request, Blog $blog): Response
    {
        $form = $this->createForm(BlogType::class, $blog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_blog_index', ['id' => $blog->getId()]);
        }

        return $this->render('admin/blog/edit.html.twig', [
            'blog' => $blog,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/{id}", name="admin_blog_delete", methods="DELETE")
     */
    public function delete(Request $request, Blog $blog): Response
    {
        if ($this->isCsrfTokenValid('delete' . $blog->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($blog);
            $em->flush();
        }

        return $this->redirectToRoute('admin_blog_index');
    }

    /**
     * @Route("/", name="blog_public_index", methods="GET")
     * renvoie tout les blog pour la vue public
     */
    public function public_index(PaginatorInterface $paginator, BlogRepository $blogRepository, UserRepository $userRepo, CategoryBlogRepository $blogCategories, Request $request): Response
    {

        $page = $paginator->paginate(
            $blogRepository->findBy([],['createdAt'=> 'desc']),
            $request->query->getInt('page', 1),
            12
    
        );
        return $this->render(
            'blog/public_show_index.html.twig',
            [
                //'blogs' => $blogRepository->findAll(),
                'blogs'=> $page,
                'categories' => $blogCategories->findAll(),
                'user' => $userRepo->findAll()
            ]
        );
    }

    /**
     * @Route("/category/{id}", name="blog_categorie_index", methods="GET")
     * renvoi toute les catgories
     */
    public function public_category_index(BlogRepository $blogRepository, CategoryBlog $id, CategoryBlogRepository $blogCategories): Response
    {

        return $this->render(
            'blog/category_public_show_index.html.twig',
            [
                'blogs' => $blogRepository->findBy(['CategoryBlog' => $id]),
                'categories' => $blogCategories->findAll(),
            ]
        );
    }

    /**
     * @Route("/counter/{id}", name="blog_count", methods="GET")
     * Ajoute une vue pour le blog
     */
    public function counter(Blog $blog)
    {

        $em = $this->getDoctrine()->getManager();
        // récuperer la valeur contenu dans la base de donnée puis je lui ajoute 1 
        $val = $blog->getCounter() + 1;
        // je set le nouveau résultat dans la base de donnée
        $blog->setCounter($val);
        //puis je transmet
        $em->flush();

        return $this->redirectToRoute('blog_index');
    }
}
