<?php

namespace App\Controller;

use App\Entity\Notations;
use App\Form\NotationsType;
use App\Repository\NotationsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/notations")
 */
class NotationsController extends AbstractController
{
    /**
     * @Route("/", name="notations_index", methods="GET")
     */
    public function index(NotationsRepository $notationsRepository): Response
    {
        return $this->render('notations/index.html.twig', ['notations' => $notationsRepository->findAll()]);
    }

    /**
     * @Route("/member/new", name="notations_new", methods="GET|POST")
     * créer une nouvelle note
     */
    public function new(Request $request): Response
    {

        $notation = new Notations();
        $form = $this->createForm(NotationsType::class, $notation);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $notation->setUsers($this->getUser());
            $em->persist($notation);
            $em->flush();

            return $this->redirectToRoute('user_show', ['id' => $this->getUser()->getId()]);
        }

        return $this->render('notations/new.html.twig', [
            //return $this->render('user/show.html.twig',[
            'notation' => $notation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="notations_show", methods="GET")
     */
    public function show(Notations $notation): Response
    {
        return $this->render('notations/show.html.twig', ['notation' => $notation]);
    }

    /**
     * @Route("/member/{id}/edit", name="notations_edit", methods="GET|POST")
     */
    public function edit(Request $request, Notations $notation): Response
    {

        $form = $this->createForm(NotationsType::class, $notation);
        $form->handleRequest($request);

        // je verifie que l'utilisateur qui veut modifier la note est bien celui qui l'a créer
        if ($notation->getUsers() === $this->getUser()) {

            if ($form->isSubmitted() && $form->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('user_show', ['id' => $this->getUser()->getId()]);
            }
        }

        return $this->render('notations/edit.html.twig', [
            'notation' => $notation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/{id}", name="notations_delete", methods="DELETE")
     */
    public function delete(Request $request, Notations $notation): Response
    {
        if ($this->isCsrfTokenValid('delete' . $notation->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($notation);
            $em->flush();
        }

        return $this->redirectToRoute('notations_index');
    }
}
